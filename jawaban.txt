1. Buat Database

create database myshop;

2. Membuat Table dalam Database
 
create table users(
id int(11) primary key auto_incroment,
name varchar(255),
email varchar(255),
password varchar(255)
);

create table categories(
id int(11) primary key auto_incroment,
name varchar(255)
);

create table items(
id int(11) primary key auto_incroment,
name varchar(255),
description varchar(255),
price int(10),
stock int(10),
category_id(8),
foreign key(category_id) references items(id)
);

3. Masukkan Data pada Table
 
insert into users(name, email, password) 
values("Jhon Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123"); 
select * from users; 

insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded") 
; 
select * from categories; 

insert into items(name, description, price, stock, category_id) 
values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
select * from items; 

4. Mengambil Data dari Database

a. select id, name, email from users;
b. select * from items where price > 1000000;
select * from items where name like 'uniklo%';
c. select items.name, items.description, items.price, items.stock, items.category_id,
categories.name from items inner join categories on
items.category_id = categories.id;

5. Mengubah Data dari Database

update items set price = 2500000 where id =3;




